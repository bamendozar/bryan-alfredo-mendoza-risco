/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author bryan
 */
public class MyThread extends Thread {
    public String Mensaje;
    public MyThread(String name){
        super(name);
        start();
    }

    @Override
    public void run() {
        ServerSocket serverSocket;
        Socket server;
        try {
            serverSocket = new ServerSocket(5555);
            while (true)
            {
                try{
                    server = serverSocket.accept();
                    DataInputStream inputStream = new DataInputStream(server.getInputStream());
                    Mensaje=inputStream.readUTF();
                    ServerInterface.txtMessages.setText(ServerInterface.txtMessages.getText()+ "\n"+ Mensaje);
                    server.close();
                }
                catch (IOException ex){
                    System.out.println (ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }    
}
