/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

/**
 *
 * @author gugle
 */
import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import javax.accessibility.AccessibleContext;
import javax.swing.*;

public class ServerInterface extends JFrame /*implements Runnable*/ {
    private final JLabel lblTitle, lblMessages; /*, lblMessage*/
    public static JTextArea txtMessages;
    private final JButton btnLimpiar; //btnSubmit,
    private final JPanel panel;
    private MyThread myThread;
    public JTextArea getMenssages(){
        return txtMessages;
    }
    
    public ServerInterface(String title){
        lblTitle    = new JLabel("Chat (Servidor)");
        lblMessages = new JLabel ("Chat: ");
        txtMessages = new JTextArea (10, 25);
        btnLimpiar  = new JButton("Limpiar");
        panel       = new JPanel();

        panel.add(lblTitle);
        panel.add(lblMessages);  panel.add(txtMessages);
        panel.add(btnLimpiar);
        
        this.setBounds(800, 350, 320, 410);
        this.add(panel);
        this.setVisible(true);
        this.setTitle(title);
        
        myThread = new MyThread ("Hilo");
    }
    
}

